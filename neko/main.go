package neko

// This package is intended for importing

import (
	"fmt"
	"strings"
)

// global variables
// true output or formatted output
var trueput bool

// newline at the end
var nl string

// Sets correct variables for formatting and newlines
func Setup(trueOutput, newlineYesOrNo bool) error {
	trueput = trueOutput
	if newlineYesOrNo {
		nl = "\n"
	} else {
		nl = ""
	}
	return nil
}

// Takes any number of strings and prints them to stdout after being formatted by neko
func Print(a ...string) {
	fmt.Print(Sprint(a...))
}

// Takes any number of strings and returns them after being formatted by neko
func Sprint(a ...string) string {
	return printListOfStrings(a...)
}

// Takes any number of strings and prints them to stdout after being formatted by neko
func printListOfStrings(a ...string) string {
	if len(a) == 0 {
		return "" + nl
	}
	var b strings.Builder
	for _, s := range a {
		b.WriteString(printString(s))
		// Add a space between different strings
		fmt.Fprintf(&b, " ")
	}
	t := RemoveLastChar(b.String()) + nl
	return t
}

// Returns a string with the last letter cut off
func RemoveLastChar(s string) string {
	return s[0 : len(s)-1]
}

// Prints a single string with neko formatting
func printString(s string) string {
	var b strings.Builder
	// Iterate over each character in the string
	for i := 0; i < len(s); i++ {
		l := string(s[i])
		// If a backslash is found, check if the next character is one to formatted
		if l == "\\" {
			f := format(string(s[i+1]))
			b.WriteString(f)
			i++
			continue
		}
		fmt.Fprintf(&b, "%s", l)
	}
	return b.String()
}

// Format allows user input to be formatted. Give it the character after a backslash
func format(s string) string {
	if trueput {
		return "\\" + s
	}
	switch s {
	case "n":
		return "\n"
	case "\\":
		return "\\"
	case "t":
		return "\t"
	case "b":
		return "\b"
	case "r":
		return "\r"
	default:
		return "\\" + s
	}
}

// TrailingNewline returns the string but guarentees it has a newline at the end of it
func TrailingNewline(s string) string {
	// Get last character of string
	l := string(s[len(s)-1])
	if l != "\n" {
		return s + "\n"
	}
	return s
}
