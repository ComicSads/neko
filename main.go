package main

import (
	"fmt"
	"gitlab.com/ComicSads/neko/neko"
	"os"
)

func main() {
	var trueOutput, newline bool
	newline = true
	// Command line flags
	var argsDone bool
	var printable []string
	for i := 1; i < len(os.Args); i++ {
		switch os.Args[i] {
		case "-t":
			if argsDone {
				printable = append(printable, os.Args[i])
				continue
			}
			trueOutput = true
		case "-n":
			if argsDone {
				printable = append(printable, os.Args[i])
				continue
			}
			newline = false
		case "-h":
			if argsDone {
				printable = append(printable, os.Args[i])
				continue
			}
			fmt.Print("Use -t to turn off formatting\n" +
				"Use -n to turn off the newline at the end of the string\n" +
				"Use -h to show this help page\n" +
				"Use -- to signify end of command line flags\n\n" +
				"Format specifiers are:\n" +
				"\\n - newline\n\\\\ - backslash\n\\t - tab\n" +
				"\\b - backspace(non-deleting)\n\\r - carriage return, go to beginning of current line (non-deleting)")
		case "--":
			argsDone = true
		default:
			printable = append(printable, os.Args[i])
		}
	}

	neko.Setup(trueOutput, newline)

	neko.Print(printable...)
}
