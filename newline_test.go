package main

import (
	. "gitlab.com/ComicSads/neko/neko"
	"testing"
)

var nlProblem = []string{
	"foo\n",
	"bar",
	"hi",
	"bye\n"}

var nlAnswers = []string{
	"foo\n",
	"bar\n",
	"hi\n",
	"bye\n"}

func TestSometimesNewlines(t *testing.T) {
	for i := 0; i < len(nlProblem); i++ {
		s := TrailingNewline(nlProblem[i])
		a := nlAnswers[i]
		if s != a {
			t.Errorf("Test %d failed\nCorrect: \"%s\"\nReturned: \"%s\"\n", i, a, s)
		}
	}
}
