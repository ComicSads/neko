// +build mage

package main

import (
	"fmt"
	"os"
	"os/exec"
	//"github.com/magefile/mage/mg" // mg contains helpful utility functions, like Deps
)

// Builds executable named "build"
func Build() error {
	cmd := exec.Command("go", "build", "-o", "build", ".")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		return err
	}
	err = test()
	if err != nil {
		fmt.Println("Tests failed!")
	}
	return nil
}

// Run all test files
func test() error {
	cmd := exec.Command("go", "test")
	return cmd.Run()
}

// Runs go install and removes build files
func Install() error {
	err := os.RemoveAll("build")
	if err != nil {
		return err
	}
	cmd := exec.Command("go", "install")
	return cmd.Run()
}

// Checks to make sure code is ready to be pushed
func Pushable() error {
	var good bool = true

	//go vet
	cmd := exec.Command("go", "vet")
	err := cmd.Run()
	if err != nil {
		fmt.Println("Go vet has failed")
		good = false
	}

	//go test
	err = test()
	if err != nil {
		fmt.Println("Tests have failed!")
		good = false
	}

	//go fmt
	val, err := needsFmt()
	if err != nil {
		fmt.Println("Go fmt has failed")
		good = false
	}
	if val {
		fmt.Println("You need to run go fmt")
		good = false
	}

	if good == true {
		fmt.Println("Ready to push!")
		return nil
	} else {
		return fmt.Errorf("Not ready to push")
	}
}

func needsFmt() (bool, error) {
	cmd := exec.Command("gofmt", "-l", ".")
	files, err := cmd.Output()
	if err != nil {
		return false, err
	}
	if len(files) == 0 {
		return false, nil
	}
	return true, nil
}
