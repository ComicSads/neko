# Introduction
Echo's lack of portability got you down? Spent too long typing to output a literal "\c" or "-n" in echo? Too lazy to learn printf's formatting strings? Maybe your just a go purist working on your operating system written entirely in go? Or (~~best~~ worst of all) you're a weeaboo who *really* likes cats.
Either way (except that last one) you've come to the right place.
Neko outputs whatever arguments its receives, and optionally formats them.

## Possible formatting options
(You can use these with the neko.FormatChar function if you so desire)
`\n: returns a newline`
`\\: returns a backslash`
`\t: returns a horizontal tab`
`\b: returns a backspace character (see more info below)`
`\r: returns a carriage return (see more infor below)`

### Backspaces and Carriage Returns
If you are anything like me, you will probably be confused at first by the way neko (and most text formatting programs i.e. printf) handles backspaces and carriage returns. If you don't know what I'm talking about, try running these three commands after installing neko
`neko 'a\bc'`
`neko 'aa\bc'`
`neko 'aa\rc'`
Surprised? For those of you losers who haven't installed neko yet, this is what the commands output:
```
-> neko 'a\bc'
c
-> neko 'aa\bc'
ac
-> neko 'aa\rc'
ca
```
Trust me, this'll make sense

### At least more sense than "this'll"
The Unicode specification has multiple control characters, some of which can be typed with neko. Such as the backspace character, with "\b", and the carriage return "\r". Unlike the backspace on your keyboard, this backspace doesn't necessarily delete characters. It's more like if you moved the cursor backwards, and had it so when you typed a letter, it would delete the letter in front of the cursor (similar to insert mode in some text editors.) Carriage return works much the same way, except it moves you back to the beginning of the line.

## Command line flags
# Why
# Use as library
