package main

import (
	. "gitlab.com/ComicSads/neko/neko"
	"testing"
)

func TestFormatPrinting(t *testing.T) {
	var s string
	Setup(false, true)

	for i := 0; i < len(problem); i++ {
		s = Sprint(problem[i])
		//print(s)
		a := answers[i] + "\n"
		if s != a {
			t.Errorf("Test %d failed\nCorrect: \"%s\"\nReturned: \"%s\"\n", i, a, s)
		}
	}
}

func TestTruePrinting(t *testing.T) {
	var s string
	Setup(true, true)

	for i := 0; i < len(problem); i++ {
		s = Sprint(problem[i])
		//print(s)
		a := tAnswer[i] + "\n"
		if s != a {
			t.Errorf("Test %d failed\nCorrect: \"%s\"\nReturned: \"%s\"\n", i, a, s)
		}
	}
}

func TestNewline(t *testing.T) {
	var s string
	Setup(false, false)

	for i := 0; i < len(problem); i++ {
		s = Sprint(problem[i])
		//print(s)
		a := answers[i]
		if s != a {
			t.Errorf("Test %d failed\nCorrect: \"%s\"\nReturned: \"%s\"\n", i, a, s)
		}
	}
}

func TestWeird(t *testing.T) {
	var s string
	Setup(false, true)

	for i := 0; i < len(weirdQs); i++ {
		s = Sprint(weirdQs[i])
		//print(s)
		a := weirdAs[i] + "\n"
		if s != a {
			t.Errorf("Test %d failed\nCorrect: \"%s\"\nReturned: \"%s\"\n", i, a, s)
		}
	}
}

var problem = []string{
	"",
	"new\\nline",
	"back\\\\slash",
	"horizontal\\ttab",
	"backwar\\bds",
	"carriage\\rreturn"}

var answers = []string{
	"",
	"new\nline",
	"back\\slash",
	"horizontal\ttab",
	"backwar\bds",
	"carriage\rreturn"}

var tAnswer = []string{
	"",
	"new\\nline",
	"back\\\\slash",
	"horizontal\\ttab",
	"backwar\\bds",
	"carriage\\rreturn"}

var weirdQs = []string{
	"ni\\cght",
	"wha\\\t's good",
	"i like \\\n backslashes\\\\n"}

var weirdAs = []string{
	"ni\\cght",
	"wha\\\t's good",
	"i like \\\n backslashes\\n"}
